#!/usr/bin/env python
# coding: utf-8

import time
import datetime
from datetime import timezone
import json
import os
import pandas as pd
from tqdm import tqdm
import requests
from sqlalchemy import create_engine
from sqlalchemy.sql import text


# In this project I will use 2 APIs that serve weather data.
# After getting the data and manipulating them, I will store them in a private database.
# After inspecting what the APIs return , I will now create some function that I will need later.

# Define function to run in tests.py
def main():
    # Function to transform unix to datetime to match datetime for open-meteo variable link.
    def unix_to_datetime(unix):
        year=str(time.gmtime(unix).tm_year)

        month=str(time.gmtime(unix).tm_mon)
        if len(month)<2:
            month='0'+month

        day=str(time.gmtime(unix).tm_mday)
        if len(day)<2:
            day='0'+day

        hour=str(time.gmtime(unix).tm_hour)
        if len(hour)<2:
            hour='0'+hour

        mnt=str(time.gmtime(unix).tm_min)
        if len(mnt)<2:
            mnt='0'+mnt

        date=year+'-'+month+'-'+day+'T'+hour+':'+mnt
        return date


    # Function to append owm_variables to the main dictionary that'll be used to create a dataframe.
    def append_owm_variables(target:dict,var:list,num:int,source:dict):
        for i in var:
            target[i].append(source['list'][num]['components'][i])



    # Function to change degrees of wind degrees (°) into wind direction
    def degrees_to_direction(x):
        wind_dir={'N':[(348.75,360.0),(0,11.25)], 'NNE':[(11.25,33.75)],
         'NE':[(33.75,56.25)], 'ENE':[(56.25,78.65)], 'E':[(78.65,101.25)],
         'ESE':[(101.25,123.75)], 'SE':[(123.75,146.25)], 'SSE':[(146.25,168.75)],
         'S':[(168.75,191.25)], 'SSW':[(191.25,213.75)], 'SW':[(213.75,236.25)],
         'WSW':[(236.25,258.75)], 'W':[(258.75,281.25)], 'WNW':[(281.25,303.75)],
         'NW':[(303.75,326.25)], 'NNW':[(326.25,348.75)]}

        for i in list(wind_dir.items()):
            try:
                for j in i[1]:
                    if j[0]<=x<j[1]:
                        return i[0]
            except:
                pass


    # Function to change wind km/h to Beaufort
    def kmh_to_bf(x):
        kmh_bf={'0':(0,1), '1':(1,5), '2':(5,11), '3':(11,19), '4':(19,28),
        '5':(28,38), '6':(38,49), '7':(49,61), '8':(61,74), '9':(74,88), '10':(88,102),
        '11':(102,117), '12':(117)}

        for i in list(kmh_bf.items()):
            try:
                if i[1][0]<=float(x)<i[1][1]:
                    return i[0]
            except:
                return '12'


    # Function to assign column 'wind_descr' in om_df, depending on windspeed_bf.
    def wind_descr(x):
        bf={'0':'Calm', '1':'Light Air', '2':'Light Breeze', '3':'Gentle Breeze',
        '4':'Moderate Breeze', '5':'Fresh Breeze', '6':'Strong Breeze',
        '7':'Near Gale', '8':'Gale', '9':'Strong Gale',
        '10':'Storm', '11':'Violent Storm', '12':'Hurricane'}

        for i in list(bf.items()):
            if x==i[0]:
                return i[1]


    # Function to transform air quality
    def air_q(x):
        air={1:'Good',2:'Fair',3:'Moderate',4:'Poor',5:'Very Poor'}
        for i in list(air.items()):
            if x==i[0]:
                return i[1]

    # After inspecting the APIs, I get the coordinates of the cities that I want to pull data for.
    # We will examine data of Athens, Thessaloniki and Heraklion.
    city_coords={'Athens':(37.9839412,23.7283052),
    'Thessaloniki':(40.63585222079499,22.944257874930322),
    'Heraklion':(35.32642603775689,25.138645407429756)}

    # Then I create the variable links and set the api key to a variable.
    # Variable link of openweathermap.org
    var_link_owm='http://api.openweathermap.org/data/2.5/air_pollution/history?lat={}&lon={}&start={}&end={}&appid={}'
    # API key for https://openweathermap.org/
    api_key=os.environ.get('API_KEY')
    api_key_owm=f'{api_key}'
    #Variable link of open-meteo.com
    var_link_om='https://archive-api.open-meteo.com/v1/archive?latitude={}&longitude={}&start_date={}&end_date={}&hourly={}'
    # API key for https://open-meteo.com/ is not needed.

    # Information:
    # Openweathermap last data entry is always 1 hour ago.
    # Open-meteo last data entry is 7 days ago.
    # In order to keep data and database intact, without inconsistencies, we run the script every day
    # and pull data from 7 days ago after the initial run.

    # Does the script run for the first time? Let's check if we get a last date entry from the main table.
    # If not, we run the script using 0 unix time in our variable links in order to pull all the available data.
    # If we get a last date entry, we use this +1 day to pull the fresh data.

    # Connect to the database and try to query the main table.
    # If that fails, we set the variable timestamp which will be used in our variable link later.

    sql_engine=os.environ.get('SQL_ENGINE')
    engine= create_engine(f'{sql_engine}')
    con = engine.connect()

    try:
        last_date_get=pd.read_sql_query("""
        select datetime
        from main
        order by datetime desc
        limit 1
        """, con)
        last_date=str(last_date_get['datetime'][0])[:10]
        year=int(last_date[:4])
        month=int(last_date[5:7])
        day=int(last_date[8:10])
        dt = datetime.datetime(year, month, day)
        timestamp = dt.replace(tzinfo=timezone.utc).timestamp()+86399
    except:
        timestamp = 0

    # Inspect what the APIs return
    # owm_data={'coord': {'lon': 25.1386, 'lat': 35.3264},
    #  'list': [{'main': {'aqi': 2},
    #    'components': {'co': 220.3,
    #     'no': 0,
    #     'no2': 1.74,
    #     'o3': 72.96,
    #     'so2': 1.42,
    #     'pm2_5': 8.1,
    #     'pm10': 9.93,
    #     'nh3': 0},
    #    'dt': 1673308800},
    #   {'main': {'aqi': 2},
    #    'components': {'co': 220.3,
    #     'no': 0,
    #     'no2': 1.56,
    #     'o3': 76.53,
    #     'so2': 1.43,
    #     'pm2_5': 7.8,
    #     'pm10': 9.39,
    #     'nh3': 0},
    #    'dt': 1673312400},...}


    # om_data={'latitude': 35.4,
    #  'longitude': 25.0,
    #  'generationtime_ms': 41.30101203918457,
    #  'utc_offset_seconds': 0,
    #  'timezone': 'GMT',
    #  'timezone_abbreviation': 'GMT',
    #  'elevation': 49.0,
    #  'hourly_units': {'time': 'iso8601', 'cloudcover': '%'},
    #  'hourly': {'time': ['2023-01-10T00:00',
    #    '2023-01-10T01:00',
    #    '2023-01-10T02:00',
    #    '2023-01-10T03:00',
    #    '2023-01-10T04:00'],
    #   'cloudcover': [16,
    #    13,
    #    8,
    #    11,
    #    16]}}



    # Dictionaries that will be filled with data and be transformed into Dataframes.
    owm_df_dict={'datetime':[],'city':[],'air_quality':[],'co':[],'no':[],'no2':[],'o3':[],'so2':[],'pm2_5':[],'pm10':[],'nh3':[]}
    om_df_dict={'datetime':[],'city':[],'temperature_2m':[],'apparent_temperature':[],
    'windspeed_10m':[],'winddirection_10m':[], 'rain':[],'snowfall':[],'cloudcover':[]}

    # List of variables to use.
    owm_variables=list(owm_df_dict.keys())[3:]
    om_variables=list(om_df_dict.keys())[2:]


    # Fill dictionaries.
    # I proceed for every city.
    for city in city_coords.items():

        # Take the variable link and format it with coordinates, start-end unix timestamp and api key. Load the content.
        static_link_owm=var_link_owm.format(city[1][0],city[1][1],int(timestamp),int(time.time()),api_key_owm)
        response=requests.get(static_link_owm)
        content=response.content
        owm_data=json.loads(content)

        # For every record I append the city, datetime (we use the function "unix to datetime" to return into readable form),
        # air quality and air pollution variants.
        for i in tqdm(range(len(owm_data['list']))):
            owm_df_dict['city'].append(city[0])
            owm_df_dict['datetime'].append(unix_to_datetime(owm_data['list'][i]['dt']))
            owm_df_dict['air_quality'].append(owm_data['list'][i]['main']['aqi'])
            append_owm_variables(owm_df_dict,owm_variables,i,owm_data)


        # For open-meteo link I need datetime in the form of YYYY-MM-DD.
        # Now I make the end date with the function I created in order to use it in the variable link.
        end_date=unix_to_datetime(time.time())[:unix_to_datetime(time.time()).find('T')] #<<< string concat to grab the first part.

        # Make the start date after grabbing the first date of the owm_df_dict we created in order to use it in the variable link.
        start_date=owm_df_dict['datetime'][0][:owm_df_dict['datetime'][0].find('T')]

        # Because of the format of the json file that open-meteo returns, we must set a trigger so that city and datetime
        # will only be imported to our dictionary only once for every city.
        trigger=1
        for var in tqdm(om_variables):
            # Take the variable link and format it with coordinates, start-end date. No API key needed. Load the content.
            static_link_om=var_link_om.format(city[1][0],city[1][1],start_date,end_date,var)
            response=requests.get(static_link_om)
            content=response.content
            om_data=json.loads(content)
            # For every variable of om_variables list we created.
            for i in range(len(om_data['hourly']['time'])):
                if trigger:
                    om_df_dict['datetime'].append(om_data['hourly']['time'][i])
                    om_df_dict['city'].append(city[0])
                    om_df_dict[var].append(om_data['hourly'][var][i])
                else:
                    om_df_dict[var].append(om_data['hourly'][var][i])

            # After the first iteration we got the city and datetime so we pull the trigger
            # that will be re applied on the next location iteration.
            trigger=0

    # Create a dataframe with data from open-meteo
    om_df=pd.DataFrame(om_df_dict)

    # Rename columns
    om_df.rename(columns = {'temperature_2m':'temp_c', 'apparent_temperature':'temp_feel_c',
    'windspeed_10m':'windspeed_bf', 'winddirection_10m':'wind_direction',
    'rain':'rain_mm','snowfall':'snowfall_cm','cloudcover':'cloudcover_percent'}, inplace = True)

    # Use functions I created earlier to manipulate data outcome
    om_df['wind_direction']=list(map(degrees_to_direction,om_df['wind_direction']))
    om_df['windspeed_bf']=list(map(kmh_to_bf,om_df['windspeed_bf']))
    om_df['wind_description']=list(map(wind_descr,om_df['windspeed_bf']))
    om_df.insert(5, 'wind_description', om_df.pop('wind_description'))


    # Create a dataframe with data from openweathermeteo
    owm_df=pd.DataFrame(owm_df_dict)

    owm_df.rename(columns={'air_quality':'air_quality_id'}, inplace=True)

    # Use functions I created earlier to manipulate data outcome
    owm_df['air_quality']=list(map(air_q,owm_df['air_quality_id']))
    owm_df.insert(3,'air_quality', owm_df.pop('air_quality'))


    # Merge the two dataframes
    final_df=pd.merge(om_df, owm_df, on=['datetime', 'city'])

    # Drop nulls and reset index
    final_df=final_df.dropna().reset_index(drop=True)


    # Function to assign city_id in order to use it later on my database as primary key on dim table.
    def city_ids(x):
        city_id={'Athens':1,'Thessaloniki':2,'Heraklion':3}
        for i in list(city_id.items()):
            if x==i[0]:
                return i[1]


    final_df['city_id']=list(map(city_ids,final_df['city']))
    final_df.insert(2,'city_id', final_df.pop('city_id'))
    final_df['air_quality']=final_df['air_quality'].rename('air_quality_id')


    # insert the final_df dataframe to 'stage.stagetable' SQL table
    with engine.connect().execution_options(autocommit=True) as conn:
        final_df.to_sql('stagetable', schema='stage', con=conn, if_exists='append', index= False)


    # For wind_description_id we use the windspeed_bf+1 as keys!
    insert_sql="""
    insert into air(
    select distinct air_quality_id, air_quality
    from stage.stagetable
    order by air_quality_id asc)
    on conflict (id) do update set air_desc=EXCLUDED.air_desc
    ;

    insert into cities(
    select distinct city_id, city
    from stage.stagetable
    order by city_id asc)
    on conflict (id) do update set city=EXCLUDED.city
    ;

    insert into wind(
    select distinct windspeed_bf+1 as wind_description_id , wind_description
    from stage.stagetable
    order by wind_description_id asc)
    on conflict (id) do update set wind_desc=EXCLUDED.wind_desc;

    insert into main(main_id,
            inserted_on,
            datetime,
            city_id,
            temp_c,
            temp_feel_c,
            windspeed_bf,
            wind_direction,
            wind_description_id,
            rain_mm,
            snowfall_cm,
            cloudcover_percent,
            air_quality_id,
            co,
            no,
            no2,
            o3,
            so2,
            pm2_5,
            pm10,
            nh3)(
    select  cast(concat(date_part('year',datetime),'0',
                            date_part('month',datetime),'0',
                            date_part('day',datetime),'0',
                            date_part('hour',datetime),'0',
                            city_id) as bigint),
            date_trunc('second',now()),
            datetime,
            city_id,
            temp_c,
            temp_feel_c,
            windspeed_bf,
            wind_direction,
            windspeed_bf+1 as wind_description_id,
            rain_mm,
            snowfall_cm,
            cloudcover_percent,
            air_quality_id,
            co,
            no,
            no2,
            o3,
            so2,
            pm2_5,
            pm10,
            nh3
    from stage.stagetable)
    on conflict (main_id) do nothing
    ;
    """
    with engine.connect().execution_options(autocommit=True) as conn:
        conn.execute(text(insert_sql))

    def disc_msg(msg):
        payload= {'content' : f'{msg}'}

        header=os.environ.get('HEADER')

        header= {'authorization': f'{header}'}

        disc_channel=os.environ.get('DISCORD')

        requests.post (f'{disc_channel}' , data= payload , headers=header)

    entries=pd.read_sql_query("""
      select count(*)
      from stage.stagetable
      """, con)

    entries_no=str(entries['count'][0])
    msg=f'Gitlab CI/CD Pipeline executed\nEntries added: {entries_no}'

    disc_msg(msg)

     # Drop Stage
    engine= create_engine(f'{sql_engine}')
    con = engine.connect()

    sql_stage = """
    drop table if exists stage.stagetable;
    """
    # execute the 'sql_stage' query
    with engine.connect().execution_options(autocommit=True) as conn:
        query = conn.execute(text(sql_stage))

if __name__ == '__main__':
    main()
