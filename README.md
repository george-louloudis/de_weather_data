# DE_Weather_Data


## Description
This project is done to contribute into a research of a respiratory diseases health center. The scientists wanted to conduct a research on their patient's existing medical record and analyze how the weather conditions and air pollution affect respiratory diseases. The problem was that they did not have any data on weather and air pollution. 

Assignee: Respiratory disease research center.

Purpose: Examine historical weather data in conjunction with patients medical records in order to find correlation between respiratory diseases, weather conditions and air pollution.

Problem: Lack of structured weather historical data that meet the criteria of analysis.

Solution: The creation of a database that consists of data needed for scientific analysis and their visualization.

## Prerequisites:

1)Data collection has to be an automated ETL process.


2)Data have to be available to analysts and researchers in a specific, given form.


3)Ability to visualize data in the form of questions.

## Solution approach

1)Find a reliable source of data.


2)Structure the implementation plan.


3)Choose the tools that will be used. 

### 1)Source of data 

APIs used: 

	OpenWeatherMap.org 	
		Air pollution API

	Open-meteo.com
		Historical Weather API

### 2)Implementation plan.

1)Build a database to store data.


2)Create a script that will call the APIs, fetch data, manipulate and transform data to meet the assignee’s criteria and store them into the database.


3)Use specific tools to make the process automated and fail-proof.

### 3)Tools used.

![IMAGE_DESCRIPTION](https://gitlab.com/george-louloudis/de_weather_data/-/raw/main/Screenshots/Tools.png)


## Usage

Airflow DAG is scheduled to run everyday at 00:30.

Steps of execution:

1) Checks if the APIs are online and serve data (api_check). If not, the process stops and they get a notification for the incident in Discord (api_down).

2) If the APIs are online and serve data, Airflow checks if the database is online(DB_check). If not, the process stops and they get a notification for the incident in Discord.

3) If the DB is online, Airflow reads the datetime of the last entry. (get_last_entry)

4) Using Xcom, the datetime gets on the next task which checks the last entry and decides if the database is empty, up-to-date or out-of-date. (check_last_entry)

5) If the database is up-to-date the process stops and they get a notification for the incident in Discord (DB_is_up_to_date)

6) If the database is out-of-date Airflow creates a stage table in the database and triggers the pipeline in GitLab.(DB_is_out_of_date>>Create_stage_table>>CI_CD_pipeline_trigger) The process will complete and the database will be fed with fresh data.

7) If the database is empy (in case the process runs for the first time or they changed the database that keeps the data) Airflow creates stage(Create_stage_table), fact and dims tables(Create_fact_dim_tables), notifies in Discord that the database is empty and that the necessary tables were created(stage_fact_dim_tables_created) and then triggers the pipeline in GitLab(CI_CD_pipeline_trigger), which will collect all the data from scratch.

Everytime the GitLab pipeline is executed they get a notification in Discord that informs them about the number of entries added into the database.

![IMAGE_DESCRIPTION](https://gitlab.com/george-louloudis/de_weather_data/-/raw/main/Screenshots/Cont_Air.png)

As a result the database is filled with the data necessary for the research.

![IMAGE_DESCRIPTION](https://gitlab.com/george-louloudis/de_weather_data/-/raw/main/Screenshots/post_pg.png)

Then we connect metabase to the database. That way data can be visualized.

![IMAGE_DESCRIPTION](https://gitlab.com/george-louloudis/de_weather_data/-/raw/main/Screenshots/Metabase.png)



# How to install.
On Linux20.04
Docker version 20.10.12
Docker compose version v2.11.2

1) Create an account on open OpenWeatherMap.com and get an API key for their historical weather data API. Set it as a GitLab CI/CD variable named API_KEY(step 11). Also get into Airflow>dags>final_project_dag>scripts>api_check.py and replace {YOUR_API_KEY} with your API key.

2) Get your Discord authentication and channel.
Check this: [https://www.youtube.com/watch?v=DArlLAq56Mo&ab_channel=CodeDict](url)    
After you grab these, set them as GitLab CI/CD variables named HEADERS and DISCORD respectively(step 11). Also, grab those and get into Airflow>dags>final_project_dag>scripts>disc_msg.py and replace them.

3) Copy the content of this repo.

4) Get in PostgresDB_pgAdmin folder and run docker compose up -d. This will make a docker container with a postgreDB and a pgAdmin GUI. You can edit the docker compose before that to your preference.

5) Using pgAdmin, connect to the database and create a database named "Weather Data Project".

6) Get in Airflow folder and run docker compose up -d. This will create all the necessary containers for Airflow. I have already included the DAG and all the necessary files for this project. You might have to alter some stuff to match your settings.(eg. If you change the name of the database, the ip of your host machine, your credentials etc. you have to modify the get_last_entry.py and db_check.py in the sqlalchemy section.)

7) Get in Metabase folder and run docker compose up -d. 

8) Create a gitlab runner manually using binary files.

9) Register the runner. (Executor: Docker , Image: Ruby:2.7)

10) Create an account on dockerhub. (Docker image repository)

11) Set Gitlab CI/CD variables:
eg. (not actual variables. use yours)    
**API_KEY:** ff89c459909b3b8b5634223683f94600  
**DISCORD:** https://discord.com/api/v9/channels/759776220123456789/messages  
**HEADER:** NDE4MTY4Mjk3ODA1MTg1MDM1.G87nVq.NrE9rn2GxlZBIA-CxrsrZiAVlsQArpzuA9KG54  
**REGISTRYPASS:** dockerhubusername      
**REGISTRYUSER:** dockerhubpassword     
**SQL_CREDENTIALS:** postgresql+psycopg2://test:test@0.0.0.0:5432/Weather Data Project

12) Create a Gitlab CI/CD pipeline trigger token.
eg.  
curl -X POST \
     --fail \
     -F token=TOKEN \
     -F ref=REF_NAME \
     https://gitlab.com/api/v4/projects/43299120/trigger/pipeline

     Replace TOKEN with your token
     Replace REF_NAME with your branch name(eg.main)

     paste the curl in Airflow>dags>final_project_dag>final_proj_dag.py on git_trigger string variable.

13) On your machine get on /etc/gitlab-runner/config.toml and change privileged = false to privileged = true. This will enable the runner to succesfully run dind(Docker in Docker).

14) Get in airflow GUI> Admin> Connections and set the a postgres connection with the database we created in order for the PostgresOperator to work.

That's it. For any question, feel free to contact me via linkedin.


## License
You are welcome to use any part of this project for your own work or projects. In that case, I would highly appreciate to mention me using my linkedin profile.

