from sqlalchemy import create_engine

get_last_entry='Get_last_date_entry_xcom_push'
disc_db='DB_down'

def db_check():
   try:
      engine = create_engine(
      'postgresql+psycopg2:'                       
      '//test:'            # username for postgres       
      'test'                 # password for postgres  
      '@0.0.0.0:5432/'     # postgres container's name and the exposed port                
      'Weather Data Project')
      con = engine.connect()
      return get_last_entry
   except:
      return disc_db
