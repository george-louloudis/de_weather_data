import requests

db_check='DB_check'
disc_api='APIs_down'

def api_check():

   response1=str(requests.get('https://api.openweathermap.org/data/2.5/air_pollution?lat=50&lon=50&appid={YOUR_API_KEY}'))
   response2=str(requests.get('https://archive-api.open-meteo.com/v1/archive?latitude=52.52&longitude=13.41&start_date=2022-12-16&end_date=2023-01-15&hourly=temperature_2m'))
   ok='<Response [200]>'
   if response1==ok and response2==ok:
      return db_check
   else:
      return disc_api
