import time

disc_initiate='DB_is_out_of_date'
create_stage='Create_stage_table'
create_fact_dim='Create_fact_dim_tables'
disc_up_to_date='DB_is_up_to_date'

def check_last_entry(ti):
   date_db=ti.xcom_pull(key='date', task_ids='Get_last_date_entry_xcom_push')
   #func to calculate 7 days ago.
   def unix_to_date(unix):
      year=str(time.gmtime(unix).tm_year)
      
      month=str(time.gmtime(unix).tm_mon)
      if len(month)<2:
         month='0'+month
    
      day=str(time.gmtime(unix).tm_mday-7)
      if len(day)<2:
         day='0'+day

      date=year+'-'+month+'-'+day
      return date
   print (date_db)
   print (unix_to_date(int(time.time())))
   if date_db==unix_to_date(int(time.time())):
      return disc_up_to_date
   elif date_db=='0':
      return create_stage,create_fact_dim
   else:
      return disc_initiate

