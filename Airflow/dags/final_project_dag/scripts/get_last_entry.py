from sqlalchemy import create_engine
import numpy as np
import pandas as pd

def get_last_entry(ti):
   try:
      engine = create_engine(
      'postgresql+psycopg2:'                       
      '//test:'            # username for postgres       
      'test'                 # password for postgres  
      '@0.0.0.0:5432/'     # postgres container's name and the exposed port                
      'Weather Data Project')
      con = engine.connect()
      last_date_get=pd.read_sql_query("""
      select datetime
      from main
      order by datetime desc
      limit 1
      """, con)
      date=str(last_date_get['datetime'][0])[:10]
      ti.xcom_push(key='date', value=date)
      
   except:
      date='0'
      ti.xcom_push(key='date', value=date)
