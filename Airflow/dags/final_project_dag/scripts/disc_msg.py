import requests
import json

def disc_msg(msg):
    payload= {'content' : f'{msg}'}
    
    header= {'authorization': '{YOUR_DISCORD_AUTHORIZATION_HEADERS}'}
    
    requests.post ('https://discord.com/api/v9/channels/{YOUR_CHAT_SERIAL_NUMBER}/messages' , data= payload , headers=header)
    
    
