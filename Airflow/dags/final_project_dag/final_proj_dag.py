from datetime import datetime
import time
from os import path
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.operators.python import BranchPythonOperator
from airflow.providers.discord.operators.discord_webhook import DiscordWebhookOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator
from final_project_dag.scripts.api_check import api_check
from final_project_dag.scripts.db_check import db_check
from final_project_dag.scripts.get_last_entry import get_last_entry
from final_project_dag.scripts.check_last_entry import check_last_entry
from final_project_dag.scripts.disc_msg import disc_msg

default_args = {"start_date": datetime(2022, 6, 11)}
searchpath = path.abspath(path.join(path.dirname(__file__), "../../../etl/final"))

with DAG("Weather Data",schedule="30 0 * * *",template_searchpath=searchpath,default_args=default_args,) as dag:

   
   disc_api='APIs_down'
   db_check_id='DB_check'
   disc_db='DB_down'
   get_last_entry_id='Get_last_date_entry_xcom_push'
   disc_up_to_date='DB_is_up_to_date'
   disc_initiate='DB_is_out_of_date'
   create_stage='Create_stage_table'
   create_fact_dim='Create_fact_dim_tables'
   initiate_id='CI_CD_pipeline_trigger'
   git_trigger=''
   
   
   api_check=BranchPythonOperator(task_id='API_check', provide_context=True, python_callable=api_check)
   
   api_down = PythonOperator(task_id=disc_api, python_callable=disc_msg,op_kwargs={"msg":'Initializing "Weather Data"...\nAPIs: Offline\nTerminating.'})
   
   db_check=BranchPythonOperator(task_id=db_check_id, provide_context=True, python_callable=db_check)
   
   get_last_entry= PythonOperator(task_id=get_last_entry_id, python_callable=get_last_entry)
   
   db_down= PythonOperator(task_id=disc_db, python_callable=disc_msg,op_kwargs={"msg":'Initializing "Weather Data"...\nAPIs: Online\nDB: Offline\nTerminating.'})
   
   check_last_entry= BranchPythonOperator(task_id='check_last_entry_xcom_pull', python_callable=check_last_entry)
   
   disc_up_to_date= PythonOperator(task_id=disc_up_to_date, python_callable=disc_msg,op_kwargs={"msg":'Initializing "Weather Data"...\nAPIs: Online\nDB: Online\nReading last entry...\nChecking last entry...\nDB is up-to-date\nTerminating.'})
   
   create_stage= PostgresOperator(task_id=create_stage,sql='create_stage.sql', postgres_conn_id='final')
   
   create_fact_dim= PostgresOperator(task_id=create_fact_dim,sql='create_f_d.sql', postgres_conn_id='final')
   
   disc_created= PythonOperator(task_id='stage_fact_dim_tables_created', python_callable=disc_msg,op_kwargs={"msg":'Initializing "Weather Data"...\nAPIs: Online\nDB: Online\nReading last entry...\nChecking last entry...\nDB is empty.\nStage,fact,dim tables created.\nTriggering CI/CD pipeline...'})
   
   disc_initiate= PythonOperator(task_id=disc_initiate, python_callable=disc_msg,op_kwargs={"msg":'Initializing "Weather Data"...\nAPIs: Online\nDB: Online\nReading last entry...\nChecking last entry...\nDB is out-dated.\nTriggering CI/CD pipeline...'})
   
   initiate= BashOperator(task_id='CI_CD_pipeline_trigger', bash_command=f'{git_trigger}',trigger_rule='one_success')
   
   
   
   api_check>>[db_check,api_down]
   db_check>>[get_last_entry,db_down]
   get_last_entry>>check_last_entry
   check_last_entry>>disc_initiate>>create_stage>>initiate
   check_last_entry>>disc_up_to_date
   check_last_entry>>[create_stage,create_fact_dim]>>disc_created>>initiate

   
