-- Create stage table
CREATE TABLE if not exists stage.stagetable (
         "datetime" timestamptz NOT NULL,
         "city" varchar(255) NOT NULL,
         "city_id" integer NOT NULL,
         "temp_c" FLOAT NOT NULL,
         "temp_feel_c" FLOAT NOT NULL,
         "windspeed_bf" integer NOT NULL,
         "wind_description" varchar(255) NOT NULL,
         "wind_direction" varchar(255) NOT NULL,
         "rain_mm" FLOAT NOT NULL,
         "snowfall_cm" FLOAT NOT NULL,
         "cloudcover_percent" integer NOT NULL,
         "air_quality_id" integer NOT NULL,
         "air_quality" varchar(255) NOT NULL,
         "co" FLOAT NOT NULL,
         "no" FLOAT NOT NULL,
         "no2" FLOAT NOT NULL,
         "o3" FLOAT NOT NULL,
         "so2" FLOAT NOT NULL,
         "pm2_5" FLOAT NOT NULL,
         "pm10" FLOAT NOT NULL,
         "nh3" FLOAT NOT NULL
      );
