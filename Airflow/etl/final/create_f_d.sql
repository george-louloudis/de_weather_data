-- Create fact and dims

BEGIN;

CREATE TABLE if not exists main (
         "main_id" bigint primary key not null,
         "inserted_on" TIMESTAMPtz NOT NULL,
         "datetime" TIMESTAMPtz NOT NULL,
         "city_id" integer NOT NULL,
         "temp_c" FLOAT NOT NULL,
         "temp_feel_c" FLOAT NOT NULL,
         "windspeed_bf" integer NOT NULL,
         "wind_direction" varchar(255) NOT NULL,
         "wind_description_id" integer NOT NULL,
         "rain_mm" FLOAT NOT NULL,
         "snowfall_cm" FLOAT NOT NULL,
         "cloudcover_percent" integer NOT NULL,
         "air_quality_id" integer NOT NULL,
         "co" FLOAT NOT NULL,
         "no" FLOAT NOT NULL,
         "no2" FLOAT NOT NULL,
         "o3" FLOAT NOT NULL,
         "so2" FLOAT NOT NULL,
         "pm2_5" FLOAT NOT NULL,
         "pm10" FLOAT NOT NULL,
         "nh3" FLOAT NOT NULL
      );
CREATE TABLE if not exists cities (
         "id" integer NOT NULL,
         "city" varchar(255) NOT NULL,
         CONSTRAINT "cities_pk" PRIMARY KEY ("id")
      );
CREATE TABLE if not exists wind (
         "id" integer NOT NULL,
         "wind_desc" varchar(255) NOT NULL,
         CONSTRAINT "wind_pk" PRIMARY KEY ("id")
      );
CREATE TABLE if not exists air (
         "id" integer NOT NULL,
         "air_desc" varchar(255) NOT NULL,
         CONSTRAINT "air_pk" PRIMARY KEY ("id")
      );

ALTER TABLE main ADD CONSTRAINT "Main_fk0" FOREIGN KEY ("city_id") REFERENCES "cities"("id");
ALTER TABLE main ADD CONSTRAINT "Main_fk1" FOREIGN KEY ("wind_description_id") REFERENCES "wind"("id");
ALTER TABLE main ADD CONSTRAINT "Main_fk2" FOREIGN KEY ("air_quality_id") REFERENCES "air"("id");

COMMIT;
