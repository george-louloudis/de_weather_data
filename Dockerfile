# set the base image with specific tag/version
FROM python:3.9

# update all packages 
RUN apt-get update 

# set up working directory inside the container
WORKDIR /app

# copy and run the requirements.txt file to install the required packages.
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# copy files from  directory to the image's filesystem
COPY weather.py .

# execute the script in the container 
CMD ["python", "weather.py"]
